package Recursividade;

import java.util.Scanner;

public class Quest1 {
	static Scanner sc = new Scanner(System.in);
	static int r;

	public static void main(String[] args) {
		int m, n;

		System.out.println("Informe o primeiro valor inteiro:");
		m = sc.nextInt();
		System.out.println("Informe o segundo valor inteiro:");
		n = sc.nextInt();
		System.out.println(CalculaMDC(m, n));
	}

	public static int CalculaMDC(int m, int n) {
		
		r = m % n;
		m = n;
		n = r;
		r = (r != 0) ? CalculaMDC(m,n) : m ;
		return r;
		
	}

	// public static int CalculaMDC(int m, int n) {
	// int r;
	// do {
	// r = m % n;
	// m = n;
	// n = r;
	// } while (r != 0);
	// return m;
	// }

}