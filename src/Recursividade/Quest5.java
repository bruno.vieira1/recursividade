package Recursividade;

public class Quest5 {
		/*5 � Desafio!!
		Torre De Hanoi.
		Objetivo: consiste em deslocar todos os discos da haste onde se encontram
		para uma haste diferente, respeitando as seguintes regras:
		1. deslocar um disco de cada vez, o qual dever� ser o do topo de uma das
		tr�s hastes;
		2. cada disco nunca poder� ser colocado sobre outro de di�metro mais
		pequeno.
		Para nosso desafio, s�o dados um conjunto de n discos com diferentes
		tamanhos e tr�s bases A, B e C.
		O problema consiste em imprimir os passos necess�rios para transferir os
		discos da base A para a base C, usando a base B como auxiliar, nunca
		colocando discos maiores sobre menores.
		Segue os passos para solu��o
		
		1� passo: Mover de A para C.
		2� passo: Mover de A para B.
		3� passo: Mover de C para B.
		4� passo: Mover de A para C.
		5� passo: Mover de B para A.
		6� passo: Mover de B para C.
		7� passo: Mover de A para C.*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
