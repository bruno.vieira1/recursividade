package Recursividade;

import java.util.Scanner;

class Quest2 {
	// 2 - Desenvolva um programa em java que calcule o fatorial de um n�mero x.
	
	static Scanner sc = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		
		System.out.println("Informe um n�mero inteiro para ser o valor de x:");
		int x = sc.nextInt();
		System.out.println(fatorial(x));
		
	}	
		
	public static int fatorial(int x) {
		int res;
		
		res = (x == 0) ? 1 : x * fatorial(x - 1);
		return res;
	}
	
}
	
