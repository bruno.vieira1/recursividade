package Recursividade;

import java.util.Scanner;

public class Quest3 {

	// 3 � Escreva em java um m�todo recursivo que receba um n�mero inteiro na
	// base decimal e realize a convers�o deste n�mero para base bin�ria.
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Informe um n�mero inteiro a ser convertido para bin�rio: ");
		int val = sc.nextInt();
		decimalParaBinario(val);

	}

	public static void decimalParaBinario(int x) {
		if (x > 0) {
			decimalParaBinario(x / 2);
			System.out.print(x % 2);

		}

	}

}
