package Recursividade;

import java.util.Scanner;

public class Quest4 {
	/*
	 * 4 - Fa�a um programa em java fazendo uso da recursividade que solicite para o
	 * usu�rio digitar um n�mero, em seguida, fa�a a soma de todos os algarismos do
	 * n�mero. Exemplos: 1981 = 1 + 9 + 8 + 1 2020 = 2 + 0 + 2 + 0
	 */
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Indome um valor: ");
		int val = sc.nextInt();
		int soma=somaAlga(val, 0);
		System.out.println(""+soma);
	}

	public static int somaAlga(int val, int soma) {
		if (val < 1) {
			return soma;
		} else {
			soma += (val % 10);
			return somaAlga(val / 10, soma);
		}

	}

}
